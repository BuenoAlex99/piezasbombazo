package vistas;

import com.github.lgooddatepicker.components.DatePicker;
import enumeradas.Productos;
import enumeradas.TipoUsuario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Cliente extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JTabbedPane tabbedPane1;
    public JPanel panel1;
    public JTextField nombreClientetxt;
    public JTextField direccionClientetxt;
    public JTextField telefonoClientetxt;
    public JTextField correoClientetxt;
    public JTextArea notasClientetxt;
    public JButton altaButtonCliente;
    public JButton modificarButtonCliente;
    public JButton eliminarButtonCliente;
    public JTextField cantidadCompra;
    public JButton altaButtonCompra;
    public JButton eliminarButtonCompra;
    public JComboBox codigoClienteCombo;
    public JTable listaClienteTabla;
    public JTable listaCompraClienteTabla;
    public DatePicker fechaCompraDatePicker;
    public JButton PDFFACTURAButton;
    public JTextField nombreUsuariotxt;
    public JTextField passwordtxt;
    public JComboBox tipoUsuarioCombo;
    public JButton modificarButtonRegistro;
    public JTable listaRegistroTabla;
    public JButton eliminarButtonRegistro;
    public JComboBox productoCombo;
    public JTextField precioCompraClientetxt;
    public JButton buscarPorNombreButton;
    public JTable tablaProductos;
    public JComboBox nombreProductoCombo;

    /*Default table model*/
    public DefaultTableModel dtmClientes;
    public DefaultTableModel dtmCompra;
    public DefaultTableModel dtmRegistro;
    public DefaultTableModel dtmProductoCliente;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Cliente() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        this.setEnumComboBox();
        setTableModels();
    }

    private void setTableModels(){
        this.dtmClientes = new DefaultTableModel();
        this.listaClienteTabla.setModel(dtmClientes);

        this.dtmCompra = new DefaultTableModel();
        this.listaCompraClienteTabla.setModel(dtmCompra);

        this.dtmRegistro = new DefaultTableModel();
        this.listaRegistroTabla.setModel(dtmRegistro);

        this.dtmProductoCliente = new DefaultTableModel();
        this.tablaProductos.setModel(dtmProductoCliente);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) {
            tipoUsuarioCombo.addItem(constant.getValor());
            tipoUsuarioCombo.setSelectedIndex(-1);
        }

        for (Productos constant : Productos.values()) {
            productoCombo.addItem(constant.getValor());
            productoCombo.setSelectedIndex(-1);
        }

        for (Productos constant : Productos.values()) {
            nombreProductoCombo.addItem(constant.getValor());
            nombreProductoCombo.setSelectedIndex(-1);
        }
    }


}

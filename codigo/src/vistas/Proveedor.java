package vistas;

import com.github.lgooddatepicker.components.DatePicker;
import enumeradas.TipoUsuario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Proveedor extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JTabbedPane tabbedPane1;
    public JPanel panel1;
    public JTextField ProveedorNombre;
    public JTextField direccionProveedortxt;
    public JTextField telefonoProveedortxt;
    public JTextField correoProveedortxt;
    public DatePicker fechaUltPedidoDatePicker;
    public JButton altaButtonProveedor;
    public JButton modificarButtonProveedor;
    public JButton eliminarButtonProveedor;
    public JTextArea notasProveedortxt;
    public JTable listaProveedorTabla;
    public JTextField nombreUsuariotxt;
    public JTextField passwordtxt;
    public JComboBox tipoUsuarioCombo;
    public JButton modificarButtonRegistro;
    public JButton eliminarButtonRegistro;
    public JTable listaRegistroTabla;

    /*Default table model*/
    public DefaultTableModel dtmProveedor;
    public DefaultTableModel dtmProducto;
    public DefaultTableModel dtmRegistro;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Proveedor() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        this.setEnumComboBox();
        setTableModels();
    }

    private void setTableModels(){
        this.dtmProveedor = new DefaultTableModel();
        this.listaProveedorTabla.setModel(dtmProveedor);

        this.dtmRegistro = new DefaultTableModel();
        this.listaRegistroTabla.setModel(dtmRegistro);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) { tipoUsuarioCombo.addItem(constant.getValor()); }
        tipoUsuarioCombo.setSelectedIndex(-1);

    }
}

package vistas;

import com.github.lgooddatepicker.components.DatePicker;
import enumeradas.Productos;
import enumeradas.TipoUsuario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Administrador extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JTabbedPane tabbedPane1;
    public JPanel panel1;
    public JTextField nombreClientetxt;
    public JTextField direccionClientetxt;
    public JTextField telefonoClientetxt;
    public JTextField correoClientetxt;
    public JTextArea notasClientetxt;
    public JTable listaClientesTabla;
    public JTextField nombreProveedortxt;
    public JTextField direccionProveedortxt;
    public JTextField telefonoProveedortxt;
    public JTextField correoProveedortxt;
    public JTextArea notasProveedortxt;
    public JTable listaProveedorTabla;
    public JTable listaProductoTabla;
    public JTextField pesoProductotxt;
    public JTextField tamañoProductotxt;
    public JTextField stockProductotxt;
    public JComboBox proveedorCombo;
    public JTextField nombreUsuariotxt;
    public JTextField passwordtxt;
    public JComboBox tipoUsuarioCombo;
    public JTextField cantidadCompra;
    public JTable listaCompraTabla;
    public JButton altaButtonCliente;
    public JButton modificarButtonCliente;
    public JButton eliminarButtonCliente;
    public JButton altaButtonProveedor;
    public JButton modificarButtonProveedor;
    public JButton eliminarButtonProveedor;
    public JButton altaButtonProducto;
    public JButton modificarButtonProducto;
    public JButton eliminarButtonProducto;
    public JButton PDFButton;
    public JButton comprarButtonCompra;
    public JButton altaButtonRegistro;
    public JButton eliminarButtonRegistro;
    public JButton modificarButtonRegistro;
    public DatePicker fechaUltPedidoDatePicker;
    public JComboBox codigoClienteCombo;
    public DatePicker fechaCompraDatePicker;
    public JTable listaRegistroTabla;
    public JButton INICIOButton;
    public JTextField precioCompraAdministrador;
    public JComboBox nombreProductoCombo;
    public JComboBox nombreCompraCombo;
    public JButton buscarPorNombreButton;
    public JComboBox buscarNombreCombo;
    public JTable buscarProductoTabla;

    /*Default table model*/
    public DefaultTableModel dtmClientes;
    public DefaultTableModel dtmProveedor;
    public DefaultTableModel dtmProducto;
    public DefaultTableModel dtmCompra;
    public DefaultTableModel dtmRegistro;
    public DefaultTableModel dtmBuscarPorNombre;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Administrador() {
        super(TITULOFRAME);
        initFrame();

    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        setEnumComboBox();
        setTableModels();
    }

    private void setTableModels(){
        this.dtmClientes = new DefaultTableModel();
        this.listaClientesTabla.setModel(dtmClientes);

        this.dtmProveedor = new DefaultTableModel();
        this.listaProveedorTabla.setModel(dtmProveedor);

        this.dtmProducto = new DefaultTableModel();
        this.listaProductoTabla.setModel(dtmProducto);

        this.dtmCompra = new DefaultTableModel();
        this.listaCompraTabla.setModel(dtmCompra);

        this.dtmRegistro = new DefaultTableModel();
        this.listaRegistroTabla.setModel(dtmRegistro);

        this.dtmBuscarPorNombre = new DefaultTableModel();
        this.buscarProductoTabla.setModel(dtmBuscarPorNombre);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) { tipoUsuarioCombo.addItem(constant.getValor()); }
        tipoUsuarioCombo.setSelectedIndex(-1);

        for(Productos constant : Productos.values()) { nombreProductoCombo.addItem(constant.getValor()); }
        nombreProductoCombo.setSelectedIndex(-1);

        for(Productos constant : Productos.values()) { nombreCompraCombo.addItem(constant.getValor()); }
        nombreCompraCombo.setSelectedIndex(-1);

        for (Productos constant : Productos.values()) { buscarNombreCombo.addItem(constant.getValor());}
        buscarNombreCombo.setSelectedIndex(-1);
    }


}

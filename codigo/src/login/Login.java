package login;

import enumeradas.TipoUsuario;
import vistas.Administrador;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JPanel panel1;
    public JButton entrarButtonLogin;
    public JComboBox tipoUsuarioCombo;
    public JTextField usuarioTxt;
    public JButton registrarButtonLogin;
    public JPasswordField passwordtxt;

    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    public Login() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        setEnumComboBox();
    }


    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) { tipoUsuarioCombo.addItem(constant.getValor()); }
        tipoUsuarioCombo.setSelectedIndex(-1);

    }


}

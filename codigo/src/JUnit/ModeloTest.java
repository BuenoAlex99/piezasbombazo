package JUnit;

import modelo.Modelo;
import vistas.Administrador;
import vistas.Cliente;
import vistas.Proveedor;

class ModeloTest {

    Administrador administrador = new Administrador();
    Proveedor proveedor = new Proveedor();
    Cliente cliente = new Cliente();

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void insertarCliente() {
    }

    @org.junit.jupiter.api.Test
    void insertarProveedor() {
    }

    @org.junit.jupiter.api.Test
    void insertarProducto() {
    }

    @org.junit.jupiter.api.Test
    void insertarCompra() {
    }

    @org.junit.jupiter.api.Test
    void insertarUsuarioLogin() {
    }

    @org.junit.jupiter.api.Test
    void insertarUsuario() {
    }

    @org.junit.jupiter.api.Test
    void modificarCliente() {
    }

    @org.junit.jupiter.api.Test
    void modificarProveedor() {
    }

    @org.junit.jupiter.api.Test
    void modificarProducto() {
    }

    @org.junit.jupiter.api.Test
    void modificarUsuario() {
    }

    @org.junit.jupiter.api.Test
    void eliminarCliente() {
    }

    @org.junit.jupiter.api.Test
    void eliminarProveedor() {
    }

    @org.junit.jupiter.api.Test
    void eliminarProducto() {
    }

    @org.junit.jupiter.api.Test
    void eliminarCompra() {
    }

    @org.junit.jupiter.api.Test
    void eliminarFactura() {
    }

    @org.junit.jupiter.api.Test
    void eliminarUsuario() {
    }

    @org.junit.jupiter.api.Test
    void consultarCliente() {
    }

    @org.junit.jupiter.api.Test
    void consultarProveedor() {
    }

    @org.junit.jupiter.api.Test
    void consultarProducto() {
    }

    @org.junit.jupiter.api.Test
    void consultarCompra() {
    }

    @org.junit.jupiter.api.Test
    void consultarRegistro() {
    }

    @org.junit.jupiter.api.Test
    void consultarRegistroCliente() {
    }

    @org.junit.jupiter.api.Test
    void consultarRegistroProveedor() {
    }

    @org.junit.jupiter.api.Test
    void consultarPorNombre() {
    }

    @org.junit.jupiter.api.Test
    void consultarPorNombreCompra() {
    }

    @org.junit.jupiter.api.Test
    void comprobarDatos() {
    }
}
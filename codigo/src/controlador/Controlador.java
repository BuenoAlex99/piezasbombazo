package controlador;


import Hash.Hash;
import enumeradas.TipoUsuario;
import login.Login;
import modelo.Modelo;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import util.Util;
import vistas.Administrador;
import vistas.Cliente;
import vistas.Proveedor;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.File;
import java.sql.*;
import java.time.LocalDate;
import java.util.Vector;


public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Administrador administrador;
    private Cliente cliente;
    private Proveedor proveedor;
    private Login login;


    public Controlador(Modelo modelo, Administrador administrador, Cliente cliente, Proveedor proveedor,
                       Login login) throws SQLException {
        this.modelo = modelo;
        this.administrador = administrador;
        this.cliente = cliente;
        this.proveedor = proveedor;
        this.login = login;
        modelo.conectar();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        addActionListenerTable();
        refrescarTodo();
    }

    public void refrescarTodo() {
        refrescarClienteAdministrador();
        refrescarProveedorAdministrador();
        refrescarProductoAdministrador();
        refrescarCompraAdministrador();
        refrescarUsuarioAdministrador();
        refrescarProductoBuscar();
        refrescarClienteCliente();
        refrescarUsuarioCliente();
        refrescarCompraCliente();
        refrescarProductoBuscarCliente();
        refrescarProveedorProveedor();
        refrescarUsuarioProveedor();
    }

    private void addActionListeners(ActionListener listener) {
        administrador.altaButtonCliente.addActionListener(listener);
        administrador.modificarButtonCliente.addActionListener(listener);
        administrador.eliminarButtonCliente.addActionListener(listener);

        administrador.altaButtonProveedor.addActionListener(listener);
        administrador.modificarButtonProveedor.addActionListener(listener);
        administrador.eliminarButtonProveedor.addActionListener(listener);

        administrador.altaButtonProducto.addActionListener(listener);
        administrador.modificarButtonProducto.addActionListener(listener);
        administrador.eliminarButtonProducto.addActionListener(listener);
        administrador.buscarPorNombreButton.addActionListener(listener);

        administrador.PDFButton.addActionListener(listener);
        administrador.comprarButtonCompra.addActionListener(listener);

        administrador.altaButtonRegistro.addActionListener(listener);
        administrador.modificarButtonRegistro.addActionListener(listener);
        administrador.eliminarButtonRegistro.addActionListener(listener);
        administrador.INICIOButton.addActionListener(listener);

        cliente.altaButtonCliente.addActionListener(listener);
        cliente.modificarButtonCliente.addActionListener(listener);
        cliente.eliminarButtonCliente.addActionListener(listener);
        cliente.altaButtonCompra.addActionListener(listener);
        cliente.eliminarButtonCompra.addActionListener(listener);
        cliente.eliminarButtonRegistro.addActionListener(listener);
        cliente.modificarButtonRegistro.addActionListener(listener);
        cliente.buscarPorNombreButton.addActionListener(listener);

        proveedor.altaButtonProveedor.addActionListener(listener);
        proveedor.modificarButtonProveedor.addActionListener(listener);
        proveedor.eliminarButtonProveedor.addActionListener(listener);
        proveedor.eliminarButtonRegistro.addActionListener(listener);
        proveedor.modificarButtonRegistro.addActionListener(listener);

        login.entrarButtonLogin.addActionListener(listener);
        login.registrarButtonLogin.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        administrador.addWindowListener(listener);
        cliente.addWindowListener(listener);
        proveedor.addWindowListener(listener);
        login.addWindowListener(listener);
    }

    private void addActionListenerTable() {
        administrador.listaClientesTabla.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = administrador.listaClientesTabla.getSelectedRow();
                administrador.nombreClientetxt.setText(String.valueOf(administrador.listaClientesTabla.getValueAt(row, 1)));
                administrador.direccionClientetxt.setText(String.valueOf(administrador.listaClientesTabla.getValueAt(row, 2)));
                administrador.telefonoClientetxt.setText(String.valueOf(administrador.listaClientesTabla.getValueAt(row, 3)));
                administrador.correoClientetxt.setText(String.valueOf(administrador.listaClientesTabla.getValueAt(row, 4)));
                administrador.notasClientetxt.setText(String.valueOf(administrador.listaClientesTabla.getValueAt(row, 5)));
            }
        });

        administrador.listaProveedorTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.listaProveedorTabla.getSelectedRow();
                administrador.nombreProveedortxt.setText(String.valueOf(administrador.listaProveedorTabla.getValueAt(row, 1)));
                administrador.direccionProveedortxt.setText(String.valueOf(administrador.listaProveedorTabla.getValueAt(row, 2)));
                administrador.telefonoProveedortxt.setText(String.valueOf(administrador.listaProveedorTabla.getValueAt(row, 3)));
                administrador.correoProveedortxt.setText(String.valueOf(administrador.listaProveedorTabla.getValueAt(row, 4)));
                administrador.fechaUltPedidoDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.listaProveedorTabla.getValueAt(row, 5))));
                administrador.notasProveedortxt.setText(String.valueOf(administrador.listaProveedorTabla.getValueAt(row, 6)));
            }
        });

        administrador.listaProductoTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.listaProductoTabla.getSelectedRow();
                administrador.nombreProductoCombo.setSelectedItem(String.valueOf(administrador.listaProductoTabla.getValueAt(row, 1)));
                administrador.pesoProductotxt.setText(String.valueOf(administrador.listaProductoTabla.getValueAt(row, 2)));
                administrador.tamañoProductotxt.setText(String.valueOf(administrador.listaProductoTabla.getValueAt(row, 3)));
                administrador.stockProductotxt.setText(String.valueOf(administrador.listaProductoTabla.getValueAt(row, 4)));
                administrador.proveedorCombo.setSelectedItem(String.valueOf(administrador.listaProductoTabla.getValueAt(row, 5)));
            }
        });

        administrador.listaCompraTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.listaCompraTabla.getSelectedRow();
                administrador.nombreCompraCombo.setSelectedItem(String.valueOf(administrador.listaCompraTabla.getValueAt(row, 1)));
                administrador.cantidadCompra.setText(String.valueOf(administrador.listaCompraTabla.getValueAt(row, 2)));
                administrador.precioCompraAdministrador.setText(String.valueOf(administrador.listaCompraTabla.getValueAt(row, 3)));
                administrador.fechaCompraDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.listaCompraTabla.getValueAt(row, 4))));
                administrador.codigoClienteCombo.setSelectedItem(String.valueOf(administrador.listaCompraTabla.getValueAt(row, 5)));
            }
        });

        administrador.listaRegistroTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.listaRegistroTabla.getSelectedRow();
                administrador.nombreUsuariotxt.setText(String.valueOf(administrador.listaRegistroTabla.getValueAt(row, 1)));
                administrador.passwordtxt.setText(String.valueOf(administrador.listaRegistroTabla.getValueAt(row, 2)));
                administrador.tipoUsuarioCombo.setSelectedItem(String.valueOf(administrador.listaRegistroTabla.getValueAt(row, 3)));
            }
        });

        cliente.listaClienteTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = cliente.listaClienteTabla.getSelectedRow();
                cliente.nombreClientetxt.setText(String.valueOf(cliente.listaClienteTabla.getValueAt(row, 1)));
                cliente.direccionClientetxt.setText(String.valueOf(cliente.listaClienteTabla.getValueAt(row, 2)));
                cliente.telefonoClientetxt.setText(String.valueOf(cliente.listaClienteTabla.getValueAt(row, 3)));
                cliente.correoClientetxt.setText(String.valueOf(cliente.listaClienteTabla.getValueAt(row, 4)));
                cliente.notasClientetxt.setText(String.valueOf(cliente.listaClienteTabla.getValueAt(row, 5)));
            }
        });

        cliente.listaCompraClienteTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = cliente.listaCompraClienteTabla.getSelectedRow();
                cliente.productoCombo.setSelectedItem(String.valueOf(cliente.listaCompraClienteTabla.getValueAt(row, 1)));
                cliente.cantidadCompra.setText(String.valueOf(cliente.listaCompraClienteTabla.getValueAt(row, 2)));
                cliente.precioCompraClientetxt.setText(String.valueOf(cliente.listaCompraClienteTabla.getValueAt(row, 3)));
                cliente.fechaCompraDatePicker.setDate(LocalDate.parse(String.valueOf(cliente.listaCompraClienteTabla.getValueAt(row, 4))));
                cliente.codigoClienteCombo.setSelectedItem(String.valueOf(cliente.listaCompraClienteTabla.getValueAt(row, 5)));
            }
        });

        cliente.listaRegistroTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = cliente.listaRegistroTabla.getSelectedRow();
                cliente.nombreUsuariotxt.setText(String.valueOf(cliente.listaRegistroTabla.getValueAt(row, 1)));
                cliente.passwordtxt.setText(String.valueOf(cliente.listaRegistroTabla.getValueAt(row, 2)));
                cliente.tipoUsuarioCombo.setSelectedItem(String.valueOf(cliente.listaRegistroTabla.getValueAt(row, 3)));
            }
        });

        proveedor.listaProveedorTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = proveedor.listaProveedorTabla.getSelectedRow();
                proveedor.ProveedorNombre.setText(String.valueOf(proveedor.listaProveedorTabla.getValueAt(row, 1)));
                proveedor.direccionProveedortxt.setText(String.valueOf(proveedor.listaProveedorTabla.getValueAt(row, 2)));
                proveedor.telefonoProveedortxt.setText(String.valueOf(proveedor.listaProveedorTabla.getValueAt(row, 3)));
                proveedor.correoProveedortxt.setText(String.valueOf(proveedor.listaProveedorTabla.getValueAt(row, 4)));
                proveedor.fechaUltPedidoDatePicker.setDate(LocalDate.parse(String.valueOf(proveedor.listaProveedorTabla.getValueAt(row, 5))));
                proveedor.notasProveedortxt.setText(String.valueOf(proveedor.listaProveedorTabla.getValueAt(row, 6)));
            }
        });

        proveedor.listaRegistroTabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = proveedor.listaRegistroTabla.getSelectedRow();
                proveedor.nombreUsuariotxt.setText(String.valueOf(proveedor.listaRegistroTabla.getValueAt(row, 1)));
                proveedor.passwordtxt.setText(String.valueOf(proveedor.listaRegistroTabla.getValueAt(row, 2)));
                proveedor.tipoUsuarioCombo.setSelectedItem(String.valueOf(proveedor.listaRegistroTabla.getValueAt(row, 3)));
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "INICIO":
                login.setVisible(true);
                administrador.setVisible(false);
                administrador.dispose();
                break;
            case "ENTRAR":
                //String contrasena = new String(login.txtContrasena.getText());

                if(comprobarRegistroVacio2()){
                    Util.showErrorAlert("Error, debes rellenar todos los campos.");
                } else {

                    String contrasenaHash = Hash.sha1(login.passwordtxt.getText());
                    boolean comprobarLogin = modelo.comprobarDatos(login.usuarioTxt.getText(), contrasenaHash,
                            String.valueOf(login.tipoUsuarioCombo.getSelectedItem()));

                    if (comprobarLogin) {
                        if (login.tipoUsuarioCombo.getSelectedItem().equals(TipoUsuario.ADMINISTRADOR.getValor())) {
                            administrador.dispose();
                            administrador.setVisible(true);
                            login.setVisible(false);
                        } else if (login.tipoUsuarioCombo.getSelectedItem().equals("PROVEEDOR")) {
                            proveedor.setVisible(true);
                            login.setVisible(false);
                        } else if (login.tipoUsuarioCombo.getSelectedItem().equals("CLIENTE")) {
                            cliente.setVisible(true);
                            login.setVisible(false);
                        }
                    }else{
                        Util.showErrorAlert("ERROR. Datos de inicio de sesión incorrectos.");
                    }
                }

                break;
            case "altaClienteAdministrador": {
                if (comprobarClienteVacio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    administrador.listaClientesTabla.clearSelection();
                } else {

                    modelo.insertarCliente(
                            administrador.nombreClientetxt.getText(),
                            administrador.direccionClientetxt.getText(),
                            Integer.parseInt(administrador.telefonoClientetxt.getText()),
                            administrador.correoClientetxt.getText(),
                            administrador.notasClientetxt.getText());
                }
                borrarCamposClientes();
                refrescarClienteAdministrador();
            }
            break;
            case "modificarClienteAdministrador":
                modelo.modificarCliente(
                        administrador.nombreClientetxt.getText(),
                        administrador.direccionClientetxt.getText(),
                        Integer.parseInt(administrador.telefonoClientetxt.getText()),
                        administrador.correoClientetxt.getText(),
                        administrador.notasClientetxt.getText(),
                        Integer.parseInt(String.valueOf(administrador.listaClientesTabla.getValueAt(
                                administrador.listaClientesTabla.getSelectedRow(), 0)))
                );
                borrarCamposClientes();
                refrescarClienteAdministrador();
                break;
            case "eliminarClienteAdministrador":
                modelo.eliminarCliente((Integer) administrador.listaClientesTabla.getValueAt(
                        administrador.listaClientesTabla.getSelectedRow(), 0));
                borrarCamposClientes();
                refrescarClienteAdministrador();
                break;
            case "altaProveedorAdministrador":
                if (comprobarProveedorVacio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    administrador.listaProveedorTabla.clearSelection();
                } else {
                    modelo.insertarProveedor(
                            administrador.nombreProveedortxt.getText(),
                            administrador.direccionProveedortxt.getText(),
                            Integer.parseInt(administrador.telefonoProveedortxt.getText()),
                            administrador.correoProveedortxt.getText(),
                            administrador.fechaUltPedidoDatePicker.getDate(),
                            administrador.notasProveedortxt.getText()
                    );

                }
                borrarCamposProveedor();
                refrescarProveedorAdministrador();
                break;
            case "eliminarProveedorAdministrador":
                modelo.eliminarProveedor((Integer) administrador.listaProveedorTabla.getValueAt(
                        administrador.listaProveedorTabla.getSelectedRow(), 0));
                borrarCamposProveedor();
                refrescarProveedorAdministrador();
                break;
            case "modificarProveedorAdminstrador":
                modelo.modificarProveedor(
                        administrador.nombreProveedortxt.getText(),
                        administrador.direccionProveedortxt.getText(),
                        Integer.parseInt(administrador.telefonoProveedortxt.getText()),
                        administrador.correoProveedortxt.getText(),
                        administrador.notasProveedortxt.getText(),
                        administrador.fechaUltPedidoDatePicker.getDate(),
                        Integer.parseInt(String.valueOf(administrador.listaProveedorTabla.getValueAt(
                                administrador.listaProveedorTabla.getSelectedRow(), 0)))
                );
                borrarCamposProveedor();
                refrescarProveedorAdministrador();
                break;
            case "altaProductoAdministrador":
                modelo.insertarProducto(
                        (String) administrador.nombreProductoCombo.getSelectedItem(),
                        Double.parseDouble(administrador.pesoProductotxt.getText()),
                        Double.parseDouble(administrador.tamañoProductotxt.getText()),
                        Integer.parseInt(administrador.stockProductotxt.getText()),
                        (String) administrador.proveedorCombo.getSelectedItem()
                );
                borrarCamposProducto();
                refrescarProductoAdministrador();
                break;
            case "modificarProductoAdministrador":
                modelo.modificarProducto(
                        String.valueOf(administrador.nombreProductoCombo.getSelectedItem()),
                        Double.parseDouble(administrador.pesoProductotxt.getText()),
                        Double.parseDouble(administrador.tamañoProductotxt.getText()),
                        Integer.parseInt(administrador.stockProductotxt.getText()),
                        String.valueOf(administrador.proveedorCombo.getSelectedItem()),
                        Integer.parseInt(String.valueOf(administrador.listaProductoTabla.getValueAt(
                                administrador.listaProductoTabla.getSelectedRow(), 0)))
                );
                borrarCamposProducto();
                refrescarProductoAdministrador();
                break;
            case "eliminarProductoAdministrador":
                modelo.eliminarProducto((Integer) administrador.listaProductoTabla.getValueAt(
                        administrador.listaProductoTabla.getSelectedRow(), 0));
                borrarCamposProducto();
                refrescarProductoAdministrador();
                break;
            case "altaCompraAdministrador":
                modelo.insertarCompra(
                        (String) administrador.nombreCompraCombo.getSelectedItem(),
                        Integer.parseInt(administrador.cantidadCompra.getText()),
                        Integer.parseInt(administrador.precioCompraAdministrador.getText()),
                        administrador.fechaCompraDatePicker.getDate(),
                        (String) administrador.codigoClienteCombo.getSelectedItem()

                );
                borrarCamposCompraAdministrador();
                refrescarCompraAdministrador();
                break;
            case "eliminarCompraAdministrador":
                modelo.eliminarCompra((Integer) administrador.listaCompraTabla.getValueAt(
                        administrador.listaCompraTabla.getSelectedRow(), 0));
                borrarCamposCompraAdministrador();
                refrescarCompraAdministrador();
                break;
            case "PDF":
                try {
                    Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/almacenesbueno_tfg", "root", "");

                    JasperReport reporte= JasperCompileManager.compileReport("jasper\\almacenbueno.jrxml");



                        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, conexion);

                        JRExporter exporter = new JRPdfExporter();

                        exporter.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_FILE,new java.io.File("jasper\\Factura.pdf"));

                        exporter.exportReport();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "altaUsuarioAdministrador":
                try {
                    if (comprobarCamposVacios()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        String contrasena = new String(administrador.passwordtxt.getText());
                        String contrasenaHash = Hash.sha1(contrasena);
                        System.out.println(contrasenaHash);
                        modelo.insertarUsuario(
                                administrador.nombreUsuariotxt.getText(),
                                contrasenaHash,
                                String.valueOf(administrador.tipoUsuarioCombo.getSelectedItem()));


                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                }
                borrarCamposRegistro();
                refrescarUsuarioAdministrador();
                break;
            case "eliminarUsuarioAdministrador":
                modelo.eliminarUsuario((Integer) administrador.listaRegistroTabla.getValueAt(
                        administrador.listaRegistroTabla.getSelectedRow(), 0));
                borrarCamposRegistro();
                refrescarUsuarioAdministrador();
                break;
            case "modificarUsuarioAdministrador":
                modelo.modificarUsuario(
                        administrador.nombreUsuariotxt.getText(),
                        administrador.passwordtxt.getText(),
                        String.valueOf(administrador.tipoUsuarioCombo.getSelectedItem()),
                        Integer.parseInt(administrador.listaRegistroTabla.getValueAt(
                                administrador.listaRegistroTabla.getSelectedRow(), 0) + "")
                );
                refrescarUsuarioAdministrador();
                borrarCamposRegistro();
                break;

            case "altaClienteCliente":
                modelo.insertarCliente(
                        cliente.nombreClientetxt.getText(),
                        cliente.direccionClientetxt.getText(),
                        Integer.parseInt(cliente.telefonoClientetxt.getText()),
                        cliente.correoClientetxt.getText(),
                        cliente.notasClientetxt.getText()
                );
                borrarCamposClientesClientes();
                refrescarClienteCliente();
                break;
            case "modificarClienteCliente":
                modelo.modificarCliente(
                        cliente.nombreClientetxt.getText(),
                        cliente.direccionClientetxt.getText(),
                        Integer.parseInt(cliente.telefonoClientetxt.getText()),
                        cliente.correoClientetxt.getText(),
                        cliente.notasClientetxt.getText(),
                        Integer.parseInt(String.valueOf(cliente.listaClienteTabla.getValueAt(
                                cliente.listaClienteTabla.getSelectedRow(), 0)))
                );
                borrarCamposClientesClientes();
                refrescarClienteCliente();
                break;
            case "eliminarClienteCliente":
                modelo.eliminarCliente((Integer) cliente.listaClienteTabla.getValueAt(
                        cliente.listaClienteTabla.getSelectedRow(), 0));
                borrarCamposClientes();
                refrescarClienteCliente();
                break;
            case "altaCompraCliente":
                modelo.insertarCompra(
                        (String) cliente.productoCombo.getSelectedItem(),
                        Integer.parseInt(cliente.cantidadCompra.getText()),
                        Integer.parseInt(cliente.precioCompraClientetxt.getText()),
                        cliente.fechaCompraDatePicker.getDate(),
                        String.valueOf(cliente.codigoClienteCombo.getSelectedItem())
                );
                borrarCamposCompraCliente();
                refrescarCompraCliente();
                break;
            case "eliminarCompraCliente":
                modelo.eliminarCompra((Integer) cliente.listaCompraClienteTabla.getValueAt(
                        cliente.listaCompraClienteTabla.getSelectedRow(), 0));
                borrarCamposCompraCliente();
                refrescarCompraCliente();
                break;
            case "eliminarUsuarioCliente":
                modelo.eliminarUsuario((Integer) cliente.listaRegistroTabla.getValueAt(
                        cliente.listaRegistroTabla.getSelectedRow(), 0));
                borrarCamposRegistroCliente();
                refrescarUsuarioCliente();
                break;
            case "modificarUsuarioCliente":
                modelo.modificarUsuario(
                        cliente.nombreUsuariotxt.getText(),
                        cliente.passwordtxt.getText(),
                        String.valueOf(cliente.tipoUsuarioCombo.getSelectedItem()),
                        Integer.parseInt(cliente.listaRegistroTabla.getValueAt(
                                cliente.listaRegistroTabla.getSelectedRow(), 0) + "")
                );
                refrescarUsuarioCliente();
                borrarCamposRegistroCliente();
                break;
            case "altaProveedorProveedor":
                if (comprobarProveedorVacio2()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    proveedor.listaProveedorTabla.clearSelection();
                } else {
                    modelo.insertarProveedor(
                            proveedor.ProveedorNombre.getText(),
                            proveedor.direccionProveedortxt.getText(),
                            Integer.parseInt(proveedor.telefonoProveedortxt.getText()),
                            proveedor.correoProveedortxt.getText(),
                            proveedor.fechaUltPedidoDatePicker.getDate(),
                            proveedor.notasProveedortxt.getText());

                }
                borrarCamposProveedorProveedor();
                refrescarProveedorProveedor();
                break;
            case "eliminarProveedorProveedor":
                modelo.eliminarProveedor((Integer) proveedor.listaProveedorTabla.getValueAt(
                        proveedor.listaProveedorTabla.getSelectedRow(), 0));
                borrarCamposProveedorProveedor();
                refrescarProveedorProveedor();
                break;
            case "modificarProveedorProveedor":
                modelo.modificarProveedor(
                        proveedor.ProveedorNombre.getText(),
                        proveedor.direccionProveedortxt.getText(),
                        Integer.parseInt(proveedor.telefonoProveedortxt.getText()),
                        proveedor.correoProveedortxt.getText(),
                        proveedor.notasProveedortxt.getText(),
                        proveedor.fechaUltPedidoDatePicker.getDate(),
                        Integer.parseInt(String.valueOf(proveedor.listaProveedorTabla.getValueAt(
                                proveedor.listaProveedorTabla.getSelectedRow(), 0)))

                );
                borrarCamposProveedorProveedor();
                refrescarProveedorProveedor();
                break;
            case "eliminarUsuarioProveedor":
                modelo.eliminarUsuario((Integer) proveedor.listaRegistroTabla.getValueAt(
                        proveedor.listaRegistroTabla.getSelectedRow(), 0));
                borrarCamposRegistroProveedor();
                refrescarUsuarioProveedor();
                break;
            case "modificarUsuarioProveedor":
                modelo.modificarUsuario(
                        proveedor.nombreUsuariotxt.getText(),
                        proveedor.passwordtxt.getText(),
                        String.valueOf(proveedor.tipoUsuarioCombo.getSelectedItem()),
                        Integer.parseInt(proveedor.listaRegistroTabla.getValueAt(
                                proveedor.listaRegistroTabla.getSelectedRow(), 0) + "")
                );
                borrarCamposRegistroProveedor();
                refrescarUsuarioProveedor();
                break;
            case "buscarPorNombre":

                String buscarProducto = (String) administrador.buscarNombreCombo.getSelectedItem();
                try {
                    ResultSet rs = modelo.consultarPorNombre(buscarProducto);
                    refrescarProductoBuscar();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "buscarProductoCliente":

                String buscarProductoCliente = (String) cliente.nombreProductoCombo.getSelectedItem();
                try {
                    ResultSet rs = modelo.consultarPorNombreCompra(buscarProductoCliente);
                    refrescarProductoBuscarCliente();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }

    public void refrescarProductoBuscar(){
        try {
            administrador.buscarProductoTabla.setModel(construirTableModelProductoBuscar(
                    modelo.consultarPorNombre((String) administrador.buscarNombreCombo.getSelectedItem())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProductoBuscar(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmBuscarPorNombre.setDataVector(data, columnNames);

        return administrador.dtmBuscarPorNombre;
    }

    public void refrescarProductoBuscarCliente(){
        try {
            cliente.tablaProductos.setModel(construirTableModelBuscarProductoCliente(
                    modelo.consultarPorNombreCompra((String) cliente.nombreProductoCombo.getSelectedItem())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelBuscarProductoCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmProductoCliente.setDataVector(data, columnNames);

        return cliente.dtmProductoCliente;
    }

    public void refrescarClienteAdministrador(){
        try {
            administrador.listaClientesTabla.setModel(construirTableModelClientes(modelo.consultarCliente()));
            administrador.codigoClienteCombo.removeAllItems();
            for(int i = 0; i < administrador.dtmClientes.getRowCount(); i++){
                administrador.codigoClienteCombo.addItem(
                        administrador.dtmClientes.getValueAt(i,1)
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelClientes(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmClientes.setDataVector(data, columnNames);

        return administrador.dtmClientes;

    }

    public void refrescarClienteCliente(){
        try {
            cliente.listaClienteTabla.setModel(construirTableModelCliente(modelo.consultarCliente()));
            for(int i = 0; i < cliente.dtmClientes.getRowCount(); i++){
                cliente.codigoClienteCombo.addItem(
                        cliente.dtmClientes.getValueAt(i,0) + " " +
                                cliente.dtmClientes.getValueAt(i,1)
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmClientes.setDataVector(data, columnNames);

        return cliente.dtmClientes;

    }

    public void refrescarProveedorProveedor(){
        try {
            proveedor.listaProveedorTabla.setModel(construirTableModelProveedorProveedor(modelo.consultarProveedor()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProveedorProveedor(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        proveedor.dtmProveedor.setDataVector(data, columnNames);

        return proveedor.dtmProveedor;

    }

    public void refrescarCompraAdministrador(){
        try {
            administrador.listaCompraTabla.setModel(construirTableModelCompraAdministrador(modelo.consultarCompra()));

        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelCompraAdministrador(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmCompra.setDataVector(data, columnNames);

        return administrador.dtmCompra;
    }

    public void refrescarProveedorAdministrador(){
        try {
            administrador.listaProveedorTabla.setModel(construirTableModelProveedor(modelo.consultarProveedor()));
            for(int i = 0; i < administrador.dtmProveedor.getRowCount(); i++){
                administrador.proveedorCombo.addItem(
                        administrador.dtmProveedor.getValueAt(i,0) + " " +
                                administrador.dtmProveedor.getValueAt(i,1)
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProveedor(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmProveedor.setDataVector(data, columnNames);

        return administrador.dtmProveedor;
    }

    public void refrescarProductoAdministrador(){

        try {
            administrador.listaProductoTabla.setModel(construirTableModelProducto(modelo.consultarProducto()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProducto(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmProducto.setDataVector(data, columnNames);

        return administrador.dtmProducto;
    }

    public void refrescarCompraCliente(){

        try {
            cliente.listaCompraClienteTabla.setModel(construirTableModelCompra(modelo.consultarCompra()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelCompra(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmCompra.setDataVector(data, columnNames);

        return cliente.dtmCompra;
    }

    public void refrescarUsuarioAdministrador(){
        try {
            administrador.listaRegistroTabla.setModel(construirTableModelUsuario(modelo.consultarRegistro()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelUsuario(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmRegistro.setDataVector(data, columnNames);

        return administrador.dtmRegistro;
    }

    public void refrescarUsuarioCliente(){
        try {
            cliente.listaRegistroTabla.setModel(construirTableModelUsuarioCliente(modelo.consultarRegistroCliente()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelUsuarioCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmRegistro.setDataVector(data, columnNames);

        return cliente.dtmRegistro;
    }

    public void refrescarUsuarioProveedor(){
        try {
            proveedor.listaRegistroTabla.setModel(construirTableModelUsuarioProveedor(modelo.consultarRegistroProveedor()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelUsuarioProveedor(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        proveedor.dtmRegistro.setDataVector(data, columnNames);

        return proveedor.dtmRegistro;
    }

    public void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    public void borrarCamposClientes(){
        administrador.nombreClientetxt.setText("");
        administrador.direccionClientetxt.setText("");
        administrador.telefonoClientetxt.setText("");
        administrador.correoClientetxt.setText("");
        administrador.notasClientetxt.setText("");
    }

    public void borrarCamposClientesClientes(){
        cliente.nombreClientetxt.setText("");
        cliente.direccionClientetxt.setText("");
        cliente.telefonoClientetxt.setText("");
        cliente.correoClientetxt.setText("");
        cliente.notasClientetxt.setText("");
    }

    public void borrarCamposProveedor(){
        administrador.nombreProveedortxt.setText("");
        administrador.direccionProveedortxt.setText("");
        administrador.telefonoClientetxt.setText("");
        administrador.correoProveedortxt.setText("");
        administrador.fechaUltPedidoDatePicker.setText("");
        administrador.notasProveedortxt.setText("");
    }

    public void borrarCamposProveedorProveedor(){
        proveedor.ProveedorNombre.setText("");
        proveedor.direccionProveedortxt.setText("");
        proveedor.telefonoProveedortxt.setText("");
        proveedor.correoProveedortxt.setText("");
        proveedor.fechaUltPedidoDatePicker.setText("");
        proveedor.notasProveedortxt.setText("");
    }

    public void borrarCamposProducto(){
        administrador.nombreProductoCombo.setSelectedItem(-1);
        administrador.pesoProductotxt.setText("");
        administrador.tamañoProductotxt.setText("");
        administrador.stockProductotxt.setText("");
        administrador.proveedorCombo.setSelectedItem(-1);
    }

    public void borrarCamposCompraAdministrador(){
        administrador.nombreCompraCombo.setSelectedItem(-1);
        administrador.cantidadCompra.setText("");
        administrador.fechaCompraDatePicker.setText("");
        administrador.codigoClienteCombo.setSelectedItem(-1);
    }

    public void borrarCamposCompraCliente(){
        cliente.productoCombo.setSelectedItem(-1);
        cliente.cantidadCompra.setText("");
        cliente.precioCompraClientetxt.setText("");
        cliente.fechaCompraDatePicker.setText("");
        cliente.codigoClienteCombo.setSelectedItem(-1);
    }

    public void borrarCamposRegistro(){
        administrador.nombreUsuariotxt.setText("");
        administrador.passwordtxt.setText("");
        administrador.tipoUsuarioCombo.setSelectedItem(-1);
    }

    public void borrarCamposRegistroCliente(){
        cliente.nombreUsuariotxt.setText("");
        cliente.passwordtxt.setText("");
        cliente.tipoUsuarioCombo.setSelectedItem(-1);
    }

    public void borrarCamposRegistroProveedor(){
        proveedor.nombreUsuariotxt.setText("");
        proveedor.passwordtxt.setText("");
        proveedor.tipoUsuarioCombo.setSelectedItem(-1);
    }

    public boolean comprobarClienteVacio(){
        return administrador.nombreClientetxt.getText().isEmpty() ||
                administrador.direccionClientetxt.getText().isEmpty() ||
                administrador.telefonoClientetxt.getText().isEmpty() ||
                administrador.correoClientetxt.getText().isEmpty() ||
                administrador.notasClientetxt.getText().isEmpty();
    }

    public boolean comprobarProveedorVacio(){
        return administrador.nombreProveedortxt.getText().isEmpty() ||
                administrador.direccionProveedortxt.getText().isEmpty() ||
                administrador.telefonoProveedortxt.getText().isEmpty() ||
                administrador.correoProveedortxt.getText().isEmpty() ||
                administrador.fechaUltPedidoDatePicker.getText().isEmpty() ||
                administrador.notasProveedortxt.getText().isEmpty();
    }

    public boolean comprobarProveedorVacio2(){
        return proveedor.ProveedorNombre.getText().isEmpty() ||
                proveedor.direccionProveedortxt.getText().isEmpty() ||
                proveedor.telefonoProveedortxt.getText().isEmpty() ||
                proveedor.correoProveedortxt.getText().isEmpty() ||
                proveedor.fechaUltPedidoDatePicker.getText().isEmpty() ||
                proveedor.notasProveedortxt.getText().isEmpty();
    }

    public boolean comprobarRegistroVacio2(){
        return login.usuarioTxt.getText().isEmpty() ||
                login.passwordtxt.getText().isEmpty() ||
                login.tipoUsuarioCombo.getSelectedIndex() == -1;
    }

    public boolean comprobarCamposVacios(){
        return administrador.nombreUsuariotxt.getText().isEmpty() ||
                administrador.passwordtxt.getText().isEmpty() ||
                administrador.tipoUsuarioCombo.getSelectedIndex() == -1;
    }


    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    /*LISTENERS IPLEMENTOS NO UTILIZADOS*/

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}

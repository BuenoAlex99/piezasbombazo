package main;


import controlador.Controlador;
import login.Login;
import modelo.Modelo;

import vistas.Administrador;
import vistas.Cliente;
import vistas.Proveedor;

import java.awt.*;
import java.sql.SQLException;

public class Principal {
    public static void main(String[] args) throws SQLException {
        Modelo modelo = new Modelo();
        Administrador vista = new Administrador();
        Cliente cliente = new Cliente();
        Proveedor proveedor = new Proveedor();
        Login login = new Login();
        Controlador controlador = new Controlador(modelo, vista, cliente, proveedor, login);
    }

}

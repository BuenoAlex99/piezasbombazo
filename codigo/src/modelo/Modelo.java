package modelo;

/**
 * Created by DAM on 11/12/2020.
 */
import vistas.Administrador;
import vistas.Cliente;
import vistas.Proveedor;

import java.sql.*;
import java.time.LocalDate;

/**
 * Created by Alex on 02/12/2019.
 */
public class Modelo {

    private static Connection conexion;



    public void conectar() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/almacenesBueno_TFG", "root", "");

    }

    // metodos botones Administrador

    public void insertarCliente(String nombre, String direccion, int telefono, String correo, String notas) {
        String consulta = "INSERT INTO cliente (nombre, direccion,telefono, correo, notas)" +
                " values (?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);

            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public void insertarProveedor(String nombre, String direccion, int telefono, String correo, LocalDate fechaUltimoPedido, String notas) {
        String consulta = "INSERT INTO proveedor(nombre, direccion,telefono, correo,fechaUltimoPedido, notas)" +
                "values (?,?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);

            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setDate(5, Date.valueOf(String.valueOf(fechaUltimoPedido)));
            sentencia.setString(6, notas);


            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    public void insertarProducto(String nombre, double peso, double tamaño, int stock, String codigoProveedor){
        String consulta = "INSERT INTO producto (nombre, peso, tamaño, stock, idProveedor) " +
                "values (?,?,?,?,?)";

        String idProveedor = (codigoProveedor.split(" ")[0]);
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setDouble(2, peso);
            sentencia.setDouble(3, tamaño);
            sentencia.setInt(4, stock);
            sentencia.setString(5, idProveedor);

            sentencia.executeUpdate();
        }catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    public void insertarCompra(String nombreProducto, int cantidad, int precio, LocalDate fechaCompra, String nombreCliente){
        String consulta = "INSERT INTO compras(nombreProducto, cantidadCompra, precio,  fechaCompra, nombre) " +
                "values(?,?,?,?,?)";
        PreparedStatement sentencia = null;

        String nombre = String.valueOf(nombreCliente.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombreProducto);
            sentencia.setInt(2, cantidad);
            sentencia.setInt(3, precio);
            sentencia.setDate(4, Date.valueOf(fechaCompra));
            sentencia.setString(5, nombre);

            sentencia.executeUpdate();
        }catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public void insertarUsuarioLogin(String nombreUsuario, String contrasena, String tipoUsuario){
        String consulta = "INSERT INTO usuarios (nombreUsuario, contrasena, tipoUsuario) "+
                "values(?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);

            sentencia.setString(1, nombreUsuario);
            sentencia.setString(2, contrasena);
            sentencia.setString(3, String.valueOf(tipoUsuario));

            if (tipoUsuario == "ADMINISTRADOR"){
                Administrador administrador = new Administrador();
                administrador.dispose();

            } else if (tipoUsuario.equalsIgnoreCase("PROVEEDOR")){
                Proveedor proveedor = new Proveedor();
                proveedor.dispose();
            } else if (tipoUsuario.equalsIgnoreCase("CLIENTE")){
                Cliente cliente = new Cliente();
                cliente.dispose();
            }
            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public void insertarUsuario(String nombre, String password, String tipoUsuario) {
        String consulta = "INSERT INTO usuarios (nombreUsuario, contrasena, tipoUsuario) " +
                "values(?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);

            sentencia.setString(1, nombre);
            sentencia.setString(2, password);
            sentencia.setString(3, String.valueOf(tipoUsuario));

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    public void modificarCliente(String nombre, String direccion, int telefono, String correo, String notas, int idCliente){
        String consulta = "UPDATE cliente SET nombre = ?, direccion = ?, telefono = ?, correo = ?, notas = ? where idCliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);
            sentencia.setInt(6, idCliente);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void modificarProveedor(String nombre, String direccion, int telefono, String correo, String notas,
                                   LocalDate fechaUltimoPedido, int idProveedor){
        String consulta = "UPDATE proveedor SET nombre = ?, direccion = ?, telefono = ?, correo = ?, notas = ?, " +
                "fechaUltimoPedido = ? where idProveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setString(3, String.valueOf(telefono));
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);
            sentencia.setDate(6, Date.valueOf(fechaUltimoPedido));
            sentencia.setInt(7, idProveedor);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void modificarProducto(String nombre, double peso, double tamaño, int stock, String proveedor,
                                  int idProducto){
        String consulta = "UPDATE producto SET nombre = ?, peso = ?, tamaño = ?, stock = ?," +
                " idproveedor = ? WHERE idProducto = ?";
        PreparedStatement sentencia = null;

        int idProveedor = Integer.valueOf(proveedor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setDouble(2, peso);
            sentencia.setDouble(3, tamaño);
            sentencia.setInt(4, stock);
            sentencia.setInt(5, idProveedor);
            sentencia.setInt(6, idProducto);

            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void modificarUsuario(String nombre, String contrasena, String tipoUsuario, int idUsuario){
        String consulta = "UPDATE usuarios SET nombreUsuario = ?, contrasena = ?, tipoUsuario = ?" +
                " where idUsuario = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, contrasena);
            sentencia.setString(3, tipoUsuario);
            sentencia.setInt(4, idUsuario);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void eliminarCliente(int idCliente){
        String consulta = "DELETE FROM cliente WHERE  idCliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idCliente);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void eliminarProveedor(int idProveedor){
        String consulta = "DELETE FROM proveedor WHERE idProveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idProveedor);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void eliminarProducto(int idProducto){
        String consulta = "DELETE FROM producto WHERE idProducto = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idProducto);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void eliminarCompra(int idCompra){
        String consulta = "DELETE FROM compras WHERE idCompra = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idCompra);
            sentencia.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void eliminarFactura(int idFactura){
        String consulta = "DELETE FROM factura WHERE idFactura = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idFactura);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void eliminarUsuario(int idUsuario){
        String consulta = "DELETE FROM usuarios WHERE idUsuario = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idUsuario);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

   public ResultSet consultarCliente() throws SQLException{
        String sentenciaSql = "SELECT idCliente, nombre, direccion, telefono, correo, notas FROM cliente";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
   }

    public ResultSet consultarProveedor() throws SQLException{
        String sentenciaSql = "SELECT * from proveedor";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    public ResultSet consultarProducto() throws SQLException{
        String sentenciaSql = "SELECT * from producto";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }



    public ResultSet consultarCompra() throws SQLException{
        String sentenciaSql = "SELECT * from compras";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    public ResultSet consultarRegistro() throws SQLException{
        String sentenciaSql = "SELECT * from usuarios";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    public ResultSet consultarRegistroCliente() throws SQLException{
        String sentenciaSQL = "SELECT * from usuarios where tipoUsuario = 'CLIENTE' and tipoUsuario = 'cliente'";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSQL);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    public ResultSet consultarRegistroProveedor() throws SQLException{
        String sentenciaSQL = "SELECT * from usuarios where tipoUsuario = 'PROVEEDOR' and tipoUsuario = 'proveedor'";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSQL);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    public ResultSet consultarPorNombre(String buscarProducto) throws SQLException{
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT nombre, count(*) FROM producto WHERE nombre = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(buscarProducto));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public ResultSet consultarPorNombreCompra(String buscarProducto) throws SQLException{
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT nombreProducto, count(*) FROM compras WHERE nombreProducto = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(buscarProducto));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public boolean comprobarDatos(String nombre, String password, String tipoUsuario) {
        PreparedStatement sentencia = null;
        try {
            String consulta = "SELECT * FROM usuarios where nombreUsuario = ? and contrasena = ? and tipoUsuario = ?";
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, password);
            sentencia.setString(3, tipoUsuario);

            ResultSet resultado = sentencia.executeQuery();
            if(resultado.next()) {
                return true;
            }
        } catch (SQLException a) {
            a.printStackTrace();
        }
        return false;
    }


}


drop database almacenesbueno_tfg;
create database almacenesBueno_TFG;
use almacenesBueno_TFG;

-- fechas alta, baja, conexion
create table usuarios(
idUsuario int primary key auto_increment,
nombreUsuario varchar(10),
contrasena varchar(100),
tipoUsuario varchar(30)
);

-- fecha ultimo pedido, totalPedidos
create table cliente(
idCliente int primary key auto_increment,
nombre varchar(150),
direccion varchar(50),
telefono int,
correo varchar(50),
notas varchar(500)
);

-- fecha ultimo pedido, totalPedidos
create table proveedor(
idProveedor int primary key auto_increment,
nombre varchar(150),
direccion varchar(50),
telefono int,
correo varchar(50),
fechaUltimoPedido date,
notas varchar(500)
);

create table producto(
idProducto int primary key auto_increment,
nombre varchar(50),
peso real,
tamaño real,
stock int,
idProveedor int
);

-- crear tablas compras, detalleCompras, ventas, detalleVentas
create table compras(
idCompra int primary key auto_increment,
nombreProducto varchar(50),
cantidadCompra int,
precio int,
fechaCompra date, 
nombre varchar(50)
);

